package scream.battleships.framework;

/**
 * @author Verhoturov Denis - Leo.Scream.
 */
public interface ControlledScreen
{
    void setScreenParent(ScreensController parent);
}
