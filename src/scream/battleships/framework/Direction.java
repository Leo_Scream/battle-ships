package scream.battleships.framework;

public enum Direction
{
	UP, RIGHT, DOWN, LEFT
}
