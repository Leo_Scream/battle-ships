package scream.battleships.start;

import javafx.application.Application;

/**
 * @author Verhoturov Denis - Leo.Scream.
 */
public class Main
{
    public static void main(String[] args)
    {
        Application.launch(BattleShips.class, args);
    }
}
